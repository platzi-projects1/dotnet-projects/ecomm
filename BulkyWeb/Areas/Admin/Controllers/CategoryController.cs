using Microsoft.AspNetCore.Mvc;
// using Microsoft.AspNetCore.Authorization;
//using Bulky.DataAccess.Data;
using Bulky.DataAccess.Repository.IRepository;
//using Bulky.DataAccess.Repository;
using Bulky.Models;
// using Bulky.Utils;

namespace BulkyWeb.Areas.Admin.Controllers
{
    [Area("Admin")]
    // [Authorize(Roles = SD.Role_Admin)]
    public class CategoryController : Controller
    {
        //private readonly ApplicationDbContext _db;
        // private readonly ICategoryRepository _CategoryRepo;
        private readonly IUnitOfWork _unitOfWork;

        public CategoryController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }


        public IActionResult Index()
        {
            List<Category> O_CategoryList = _unitOfWork.Category.GetAll().ToList();
            return View(O_CategoryList.OrderBy(c => c.Id));
        }

        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Create(Category O_Cat)
        {
            if(O_Cat.Name == O_Cat.DisplayOrder.ToString()) {
                ModelState.AddModelError("name", "Display Order cannot exactly match the name");
            }
            if(O_Cat.Name == "test") {
                ModelState.AddModelError("", "test is an invalid name for a Category");
            }
            if(ModelState.IsValid)
            {
                _unitOfWork.Category.Add(O_Cat);
                _unitOfWork.Save();
                TempData["success"] = "Category created successfully";
                return RedirectToAction("Index");
            }
            return View(O_Cat);
        }

        public IActionResult Edit(int? id)
        {
            if (id is null && id == 0) {
                return NotFound();
            }
            Category? O_Cat = _unitOfWork.Category.Get(c => c.Id == id);

            if (O_Cat == null) {
                return NotFound();
            }
            return View(O_Cat);
        }

        [HttpPost]
        public IActionResult Edit(Category O_Cat)
        {
            if (O_Cat is null) {
                return NotFound();
            }

            if (ModelState.IsValid) {
                _unitOfWork.Category.Update(O_Cat);
                _unitOfWork.Save();
                TempData["success"] = "Category has been Updated";

                return RedirectToAction("Index");
            }
            return View(O_Cat);
        }

        public IActionResult Delete(int? id)
        {
            if (id is null && id == 0) {
                return NotFound();
            }
            Category? O_Cat = _unitOfWork.Category.Get(c => c.Id == id);

            if (O_Cat == null) {
                return NotFound();
            }
            return View(O_Cat);
        }

        [HttpPost, ActionName("Delete")]
        public IActionResult DeletePOST(int? id)
        {
            Category? O_Cat = _unitOfWork.Category.Get(c => c.Id == id);

            if (O_Cat is null) {
                return NotFound();
            }

            if (ModelState.IsValid) {
                _unitOfWork.Category.Remove(O_Cat);
                _unitOfWork.Save();
                TempData["success"] = "Category has been Removed";
            }

            return RedirectToAction("Index");
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View("Error!");
        }
    }
}
