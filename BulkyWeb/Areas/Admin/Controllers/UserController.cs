using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Bulky.Models;
using Bulky.Models.ViewModels;
using Bulky.DataAccess.Repository.IRepository;
using Bulky.DataAccess.Data;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Data;
using Bulky.Utils;

namespace BulkyWeb.Areas.Admin.Controllers;

[Area("Admin")]
// [Authorize(Roles = SD.Role_Admin)]
public class UserController : Controller
{
    private readonly IUnitOfWork? _unitOfWork;
    private readonly ApplicationDbContext? _db;
    private readonly UserManager<IdentityUser> _userManager;
    private readonly RoleManager<IdentityRole> _roleManager;

    public UserController(
        ApplicationDbContext db, 
        IUnitOfWork unitOfWork,
        UserManager<IdentityUser> userManager,
        RoleManager<IdentityRole> roleManager
    )
    {
        _unitOfWork = unitOfWork;
        _db = db;
        _roleManager = roleManager;
        _userManager = userManager;
    }

    public IActionResult Index()
    {
        return View();
    }

    public IActionResult RoleManagement(string userId) {

        var roleId = _db?.UserRoles?.FirstOrDefault(u => u.UserId == userId).RoleId;

        RoleManagementVM roleVM = new RoleManagementVM() {
            ApplicationUser = _unitOfWork.ApplicationUser.Get(u => u.Id == userId, includeProperties:"Company"),
            RoleList = _roleManager.Roles.Select(i => new SelectListItem {
                Text = i.Name,
                Value = i.Name
            }),
            CompanyList = _unitOfWork.Company.GetAll().Select(i => new SelectListItem {
                Text = i.Name,
                Value = i.Id.ToString()
            }),
        };

        roleVM.ApplicationUser.Role = _userManager.GetRolesAsync(_unitOfWork.ApplicationUser.Get(u=>u.Id==userId))
                .GetAwaiter().GetResult().FirstOrDefault();
        return View(roleVM);
    }
    [HttpPost]
    public IActionResult RoleManagement(RoleManagementVM roleManagementVM) 
    {
        var roleId = _db?.UserRoles?.FirstOrDefault(u => u.UserId == roleManagementVM.ApplicationUser.Id).RoleId;
        string oldRole  = _userManager.GetRolesAsync(_unitOfWork.ApplicationUser.Get(
            u => u.Id == roleManagementVM.ApplicationUser.Id)).GetAwaiter().GetResult().FirstOrDefault();

        ApplicationUser applicationUser = _unitOfWork.ApplicationUser.Get(u => u.Id == roleManagementVM.ApplicationUser.Id);

        if (!(roleManagementVM.ApplicationUser?.Role == oldRole)) {
            //a role was updated            

            if (roleManagementVM.ApplicationUser?.Role == SD.Role_Company) {
                applicationUser.CompanyId = roleManagementVM.ApplicationUser.CompanyId;
            }
            if (oldRole == SD.Role_Company) {
                applicationUser.CompanyId = null;
            }            

            _unitOfWork.ApplicationUser.Update(applicationUser);
            _unitOfWork.Save();

            _userManager.RemoveFromRoleAsync(applicationUser, oldRole).GetAwaiter().GetResult();
            _userManager.AddToRoleAsync(applicationUser, roleManagementVM.ApplicationUser?.Role).GetAwaiter().GetResult();

        }
        else {
            if(oldRole==SD.Role_Company && applicationUser.CompanyId != roleManagementVM.ApplicationUser?.CompanyId) {
                applicationUser.CompanyId = roleManagementVM.ApplicationUser?.CompanyId;
                _unitOfWork.ApplicationUser.Update(applicationUser);
                _unitOfWork.Save();
            }
        }

        return RedirectToAction("Index");
    }


    #region API CALLS

    [HttpGet]
    public IActionResult GetAll()
    {
        IEnumerable<ApplicationUser> outputUser = new List<ApplicationUser>();
        if (_unitOfWork is not null && _db is not null)
        {
            List<ApplicationUser> O_User = _unitOfWork.ApplicationUser.GetAll(includeProperties:"Company").ToList();

            var userRoles = _db.UserRoles.ToList();
            var roles = _db.Roles.ToList();

            foreach(var user in O_User) {                

                var roleId = userRoles.FirstOrDefault(u => u.UserId == user.Id).RoleId;
                //user.Role = roles.FirstOrDefault(u => u.Id == roleId).Name;

                if (user.Company == null) {
                    user.Company = new Company() {
                        Name = ""
                    };
                }
            }
            outputUser = O_User;
        }

        return Json(
            new {
                data = outputUser
            }
        );
    }

    [HttpPost]
    public IActionResult LockUnlock([FromBody]string id)
    {
        if (id is not null)
        {
            var objFromDb = _db.ApplicationUsers.FirstOrDefault(u => u.Id == id);

            if( objFromDb is null )
            {
                return Json( new { success = false, message = "Error while locking/unlocking" } );
            }

            if ( objFromDb.LockoutEnd is not null && objFromDb.LockoutEnd > DateTime.UtcNow)
            {
                // User is currently locked and we need to unlock them
                objFromDb.LockoutEnd = DateTime.UtcNow;
            }
            else
            {
                objFromDb.LockoutEnd = DateTime.UtcNow.AddYears(1000);
            }

            _db.SaveChanges();



        }

        return Json(
                new {
                    success = true,
                    message = "Deleted successfully"
                });
        // return Json(
        //     new {
        //         success = false,
        //         message = "You have not provide a valid book product id"
        //     });
    }

    #endregion
}
