using Microsoft.AspNetCore.Mvc;
using Bulky.Models;
using Bulky.Models.ViewModels;
using Bulky.DataAccess.Repository.IRepository;
using System.Security.Claims;
using Stripe;
using Stripe.Checkout;
using Microsoft.AspNetCore.Authorization;
using Bulky.Utils;

namespace BulkyWeb.Areas.Admin.Controllers;

[Area("Admin")]
// [Authorize]
public class OrderController : Controller
{


    private readonly IUnitOfWork _unitOfWork;

    [BindProperty]
    public OrderVM? orderVM { get; set; }

    public OrderController(IUnitOfWork unitOfWork)
    {
        _unitOfWork = unitOfWork;
    }


    public IActionResult Index()
    {
        return View();
    }


    public IActionResult Details(int orderId)
    {
        orderVM = new ()
        {
            OrderHeader = _unitOfWork.OrderHeader.Get(o => o.Id == orderId, includeProperties:"ApplicationUser"),
            OOrderDetailList = _unitOfWork.OrderDetail.GetAll(d => d.OrderHeaderId == orderId, includeProperties:"BookProduct")
        };
        return View(orderVM);
    }

    [Authorize(Roles = SD.Role_Admin + "," + SD.Role_Employee)]
    [HttpPost]
    public IActionResult UpdateOrderDetails()
    {
        OrderHeader outputOrderHeader = new OrderHeader();

        if (orderVM?.OrderHeader is not null)
        {
            var oh = _unitOfWork.OrderHeader.Get(o => o.Id == orderVM.OrderHeader.Id);
            oh.Name = orderVM.OrderHeader.Name;
            oh.PhoneNumber = orderVM.OrderHeader.PhoneNumber;
            oh.StreetAddress = orderVM.OrderHeader.StreetAddress;
            oh.City = orderVM.OrderHeader.City;
            oh.State = orderVM.OrderHeader.State;
            oh.PostalCode = orderVM.OrderHeader.PostalCode;

            if (!String.IsNullOrEmpty(orderVM.OrderHeader.Carrier))
            {
                oh.Carrier = orderVM.OrderHeader.Carrier;
            }

            if (!String.IsNullOrEmpty(orderVM.OrderHeader.TrackingNumber))
            {
                oh.Carrier = orderVM.OrderHeader.TrackingNumber;
            }
            outputOrderHeader = oh;
            _unitOfWork.OrderHeader.Update(outputOrderHeader);
            _unitOfWork.Save();

            TempData["Success"] = "Order Details Updated Successfully";

        }

        return RedirectToAction(nameof(Details), new { orderId = outputOrderHeader.Id });

    }

    [ActionName("Details")]
    [HttpPost]
    public IActionResult Details_PAY_NOW()
    {

        if (orderVM?.OrderHeader is not null)
        {
            orderVM.OrderHeader = _unitOfWork.OrderHeader
                .Get(u => u.Id == orderVM.OrderHeader.Id, includeProperties: "ApplicationUser");
            orderVM.OOrderDetailList = _unitOfWork.OrderDetail
                .GetAll(u => u.OrderHeaderId == orderVM.OrderHeader.Id, includeProperties: "Product");

            //stripe logic
            var domain = Request.Scheme + "://" + Request.Host.Value + "/";
            var options = new SessionCreateOptions {
                SuccessUrl = domain + $"admin/order/PaymentConfirmation?orderHeaderId={orderVM.OrderHeader.Id}",
                CancelUrl = domain + $"admin/order/details?orderId={orderVM.OrderHeader.Id}",
                LineItems = new List<SessionLineItemOptions>(),
                Mode = "payment",
            };

            foreach (var item in orderVM.OOrderDetailList) {
                var sessionLineItem = new SessionLineItemOptions {
                    PriceData = new SessionLineItemPriceDataOptions {
                        UnitAmount = (long)(item.Price * 100), // $20.50 => 2050
                        Currency = "usd",
                        ProductData = new SessionLineItemPriceDataProductDataOptions {
                            Name = item.BookProduct.Title
                        }
                    },
                    Quantity = item.Quantity
                };
                options.LineItems.Add(sessionLineItem);
            }


            var service = new SessionService();
            Session session = service.Create(options);
            _unitOfWork.OrderHeader.UpdateStripePaymentId(orderVM.OrderHeader.Id, session.Id, session.PaymentIntentId);
            _unitOfWork.Save();
            Response.Headers.Add("Location", session.Url);
        }
        return new StatusCodeResult(303);
    }

    public IActionResult PaymentConfirmation(int orderHeaderId)
    {

        OrderHeader orderHeader = _unitOfWork.OrderHeader.Get(u => u.Id == orderHeaderId);

        if (orderHeader.PaymentStatus == SD.PaymentStatusDelayedPayment)
        {
            //this is an order by company

            var service = new SessionService();
            Session session = service.Get(orderHeader.SessionId);

            if (session.PaymentStatus.ToLower() == "paid") {
                _unitOfWork.OrderHeader.UpdateStripePaymentId(orderHeaderId, session.Id, session.PaymentIntentId);
                _unitOfWork.OrderHeader.UpdateStatus(orderHeaderId, orderHeader.OrderStatus, SD.PaymentStatusApproved);
                _unitOfWork.Save();
            }
        }

        return View(orderHeaderId);
    }

    [Authorize(Roles = SD.Role_Admin + "," + SD.Role_Employee)]
    [HttpPost]
    public IActionResult StartProcessing()
    {
        _unitOfWork.OrderHeader.UpdateStatus(orderVM.OrderHeader.Id, SD.StatusInProcess);
        _unitOfWork.Save();

        TempData["Success"] = "Order Details Updated Successfully";

        return RedirectToAction(nameof(Details), new { orderId = orderVM.OrderHeader.Id });
    }


    [Authorize(Roles = SD.Role_Admin + "," + SD.Role_Employee)]
    [HttpPost]
    public IActionResult ShipOrder()
    {
        OrderHeader outputOrderHeader = new OrderHeader();

        if (orderVM?.OrderHeader is not null)
        {
            var oh = _unitOfWork.OrderHeader.Get(o => o.Id == orderVM.OrderHeader.Id);
            oh.TrackingNumber = orderVM.OrderHeader.TrackingNumber;
            oh.Carrier = orderVM.OrderHeader.Carrier;
            oh.OrderStatus = SD.StatusShipped;
            oh.ShippingDate = DateTime.UtcNow;

            if (oh.PaymentStatus == SD. PaymentStatusDelayedPayment)
            {
                oh.PaymentDueDate = DateTime.UtcNow.AddDays(30);
            }

            outputOrderHeader = oh;
        }

        _unitOfWork.OrderHeader.Update(outputOrderHeader);
        _unitOfWork.Save();

        TempData["Success"] = "Order Shipped Successfully";

        return RedirectToAction(nameof(Details), new { orderId = orderVM.OrderHeader.Id });
    }


    [HttpPost]
    [Authorize(Roles = SD.Role_Admin + "," + SD.Role_Employee)]
    public IActionResult CancelOrder()
    {

        OrderHeader outputOrderHeader = new OrderHeader();

        if (orderVM?.OrderHeader is not null)
        {
            var orderHeader = _unitOfWork.OrderHeader.Get(u => u.Id == orderVM.OrderHeader.Id);

            if (orderHeader.PaymentStatus == SD.PaymentStatusApproved) {
                var options = new RefundCreateOptions {
                    Reason = RefundReasons.RequestedByCustomer,
                    PaymentIntent = orderHeader.PaymentIntentId
                };

                var service = new RefundService();
                Refund refund = service.Create(options);

                _unitOfWork.OrderHeader.UpdateStatus(orderHeader.Id, SD.StatusCancelled, SD.StatusRefunded);
            }
            else {
                _unitOfWork.OrderHeader.UpdateStatus(orderHeader.Id, SD.StatusCancelled, SD.StatusCancelled);
            }
            _unitOfWork.Save();
            outputOrderHeader = orderHeader;
        }
        TempData["Success"] = "Order Cancelled Successfully.";
        return RedirectToAction(nameof(Details), new { orderId = outputOrderHeader.Id });

    }


    #region API CALLS

    [HttpGet]
    public IActionResult GetAll(string status)
    {

        IEnumerable<OrderHeader> O_OrderHeaders;


        if (User.IsInRole(SD.Role_Admin) || User.IsInRole(SD.Role_Employee))
        {
            O_OrderHeaders = _unitOfWork.OrderHeader.GetAll(includeProperties:"ApplicationUser").ToList();
        }
        else
        {
            var claimsIdentity = (ClaimsIdentity)User.Identity;
            var claim_userId = claimsIdentity.FindFirst(ClaimTypes.NameIdentifier).Value;

            O_OrderHeaders = _unitOfWork.OrderHeader.GetAll(u => u.ApplicationUserId == claim_userId, includeProperties: "ApplicationUser");
        }


        switch (status) {
            case "pending":
                O_OrderHeaders = O_OrderHeaders.Where(u => u.PaymentStatus == SD.PaymentStatusPending);
                break;
            case "inprocess":
                O_OrderHeaders = O_OrderHeaders.Where(u => u.OrderStatus == SD.StatusInProcess);
                break;
            case "completed":
                O_OrderHeaders = O_OrderHeaders.Where(u => u.OrderStatus == SD.StatusShipped);
                break;
            case "approved":
                O_OrderHeaders = O_OrderHeaders.Where(u => u.OrderStatus == SD.StatusApproved);
                break;
            default:
                break;
        }

        return Json(
            new {
                data = O_OrderHeaders
            }
        );
    }

    #endregion
}
