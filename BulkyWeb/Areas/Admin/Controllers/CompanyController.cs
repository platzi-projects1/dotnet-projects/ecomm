using Microsoft.AspNetCore.Mvc;
using Bulky.Models;
using Bulky.DataAccess.Repository.IRepository;
// using Bulky.Utils;

namespace BulkyWeb.Areas.Admin.Controllers;

[Area("Admin")]
// [Authorize(Roles = SD.Role_Admin)]
public class CompanyController : Controller
{
    private readonly ILogger<CompanyController> _logger;
    private readonly IUnitOfWork _unitOfWork;

    public CompanyController(ILogger<CompanyController> logger, IUnitOfWork unitOfWork)
    {
         _logger = logger;
        _unitOfWork = unitOfWork;
    }

    public IActionResult Index()
    {
        List<Company> O_CompanyList = _unitOfWork.Company.GetAll().ToList();

        return View(O_CompanyList.OrderBy(b => b.Id));
    }

    public IActionResult Upsert(int? id)
    {
        if (id == null || id == 0)
        {
            //Create
            return View(new Company());

        }
        else
        {
            //Update
            Company O_Company = _unitOfWork.Company.Get(b => b.Id == id);
            return View(O_Company);
        }
    }

    [HttpPost]
    public IActionResult Upsert(Company O_Company)
    {
        if(ModelState.IsValid)
        {
            if (O_Company.Id == 0)
            {
                _unitOfWork.Company.Add(O_Company);
                TempData["success"] = "Company created successfully";
            }
            else
            {
                _unitOfWork.Company.Update(O_Company);
                TempData["success"] = "Company has been successfully Updated";
            }

            _unitOfWork.Save();
            return RedirectToAction("Index");
        }

        return View(O_Company);
    }

    #region API CALLS

    [HttpGet]
    public IActionResult GetAll()
    {
        List<Company> O_Company = _unitOfWork.Company.GetAll().ToList();
        return Json(
            new {
                data = O_Company
            }
        );
    }

    [HttpDelete]
    public IActionResult Delete(int? id)
    {
        if (id is not null)
        {
            Company companyToBeDeleted = _unitOfWork.Company.Get(b => b.Id == id);

            if ( companyToBeDeleted is null )
            {
                return Json(
                    new {
                        success = false,
                        message = "Error while deleting"
                    });
            }

            _unitOfWork.Company.Remove(companyToBeDeleted);
            _unitOfWork.Save();
            return Json(
                new {
                    success = true,
                    message = "Book has been deleted successfully"
                });

        }
        return Json(
            new {
                success = false,
                message = "You have not provide a valid book product id"
            });
    }

    #endregion
}
