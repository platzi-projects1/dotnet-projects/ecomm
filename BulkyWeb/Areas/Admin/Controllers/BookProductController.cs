using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
// using Microsoft.AspNetCore.Authorization;
using Bulky.Models;
// using Bulky.Utils;
using Bulky.Models.ViewModels;
using Bulky.DataAccess.Repository.IRepository;

namespace BulkyWeb.Areas.Admin.Controllers
{
    [Area("Admin")]
    // [Authorize(Roles = SD.Role_Admin)]
    public class BookProductController : Controller
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IWebHostEnvironment _webHostEnvironment;

        public BookProductController(IUnitOfWork unitOfWork, IWebHostEnvironment webHostEnvironment)
        {
            _unitOfWork = unitOfWork;
            _webHostEnvironment = webHostEnvironment;
        }

        public IActionResult Upsert(int? id)
        {
            ProductVM productVM = new()
            {
                Product = new BookProduct(),
                CategoryList = _unitOfWork.Category.GetAll().Select(
                    u => new SelectListItem {
                        Text = u.Name,
                        Value = u.Id.ToString()
                    }
                )
            };

            if (id == null || id == 0)
            {
                //Create
                return View(productVM);

            }
            else
            {
                //Update
                productVM.Product = _unitOfWork.BookProduct.Get(b => b.Id == id, includeProperties : "ProductImages");
                return View(productVM);
            }

        }

        [HttpPost]
        public IActionResult Upsert(ProductVM BookVM, List<IFormFile>? files)
        {
            if(ModelState.IsValid)
            {
                if (BookVM.Product.Id == 0)
                {
                    _unitOfWork.BookProduct.Add(BookVM.Product);
                }
                else
                {
                    _unitOfWork.BookProduct.Update(BookVM.Product);
                }

                _unitOfWork.Save();

                string wwwRootPath = _webHostEnvironment.WebRootPath;

                if (files is not null)
                {
                    foreach (IFormFile img_file in files)
                    {
                        string filename = Guid.NewGuid().ToString() + Path.GetExtension(img_file.FileName);
                        string productPath = @"images/product-" + BookVM.Product.Id;
                        string finalPath = Path.Combine(wwwRootPath, productPath);

                        if (!Directory.Exists(finalPath))
                            Directory.CreateDirectory(finalPath);


                        using (var fileStream = new FileStream(Path.Combine(finalPath, filename), FileMode.Create))
                        {
                            img_file.CopyTo(fileStream);
                        }

                        ProductImage productImage = new ()
                        {
                            ImageUrl=@"/" + productPath + @"/" + filename,
                            ProductId = BookVM.Product.Id,
                        };

                        if ( BookVM.Product.ProductImages is null )
                            BookVM.Product.ProductImages = new List<ProductImage>();

                        BookVM.Product.ProductImages.Add(productImage);
                    }

                    _unitOfWork.BookProduct.Update(BookVM.Product);
                    _unitOfWork.Save();
                }

                TempData["success"] = "Product created/updated successfully";
                return RedirectToAction("Index");
            }
            else
            {                
                BookVM.CategoryList = _unitOfWork.Category.GetAll().Select(
                    u => new SelectListItem {
                        Text = u.Name,
                        Value = u.Id.ToString()
                    }
                );
            }
            return View(BookVM);
        }

        public IActionResult Index()
        {
            List<BookProduct> O_BookProductList = _unitOfWork.BookProduct.GetAll(includeProperties: "Category").ToList();
            
            return View(O_BookProductList.OrderBy(b => b.Id));
        }

        public IActionResult DeleteImage(int imageId)
        {
            var imageToBeDeleted = _unitOfWork.ProductImage.Get(img => img.Id == imageId);

            string wwwRootPath = _webHostEnvironment.WebRootPath;

            if ( imageToBeDeleted is not null )
            {
                if (!string.IsNullOrEmpty(imageToBeDeleted.ImageUrl))
                {

                    var oldImagePath = wwwRootPath + imageToBeDeleted.ImageUrl;

                    if (System.IO.File.Exists(oldImagePath))
                    {
                        System.IO.File.Delete(oldImagePath);
                    }
                    _unitOfWork.ProductImage.Remove(imageToBeDeleted);

                }

                _unitOfWork.Save();
            }

            TempData["success"] = "Deleted successfully";
            return RedirectToAction(nameof(Upsert), new { id = imageToBeDeleted?.ProductId  });
        }

        #region API CALLS

        [HttpGet]
        public IActionResult GetAll()
        {
            List<BookProduct> O_BookList = _unitOfWork.BookProduct.GetAll(includeProperties: "Category").ToList();
            return Json(
                new {
                    data = O_BookList
                }
            );
        }

        [HttpDelete]
        public IActionResult Delete(int? id)
        {
            if (id is not null)
            {
                BookProduct bookToBeDeleted = _unitOfWork.BookProduct.Get(b => b.Id == id);

                if ( bookToBeDeleted is null )
                {
                    return Json(
                        new {
                            success = false,
                            message = "Error while deleting"
                        });
                }

                string productPath = @"images/product-" + id;
                string finalPath = Path.Combine(_webHostEnvironment.WebRootPath, productPath);

                        if (Directory.Exists(finalPath))
                        {
                            string[] filePaths = Directory.GetFiles(finalPath);

                            foreach (string filePath in filePaths) {
                                System.IO.File.Delete(filePath);
                            }

                            Directory.Delete(finalPath);
                        }


                _unitOfWork.BookProduct.Remove(bookToBeDeleted);
                _unitOfWork.Save();
                return Json(
                    new {
                        success = true,
                        message = "Book has been deleted successfully"
                    });

            }
            return Json(
                    new {
                        success = false,
                        message = "You have not provide a valid book product id"
                    });
        }

        #endregion

    }
}
