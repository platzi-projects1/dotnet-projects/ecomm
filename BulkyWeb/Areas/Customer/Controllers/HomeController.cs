﻿using System.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using BulkyWeb.Models;
using Bulky.Models;
using Bulky.Utils;
using Bulky.DataAccess.Repository.IRepository;
using System.Security.Claims;
using Microsoft.AspNetCore.Authorization;

namespace BulkyWeb.Areas.Customer.Controllers;

[Area("Customer")]
public class HomeController : Controller
{
    private readonly ILogger<HomeController> _logger;
    private readonly IUnitOfWork _unitOfWork;

    public HomeController(ILogger<HomeController> logger, IUnitOfWork unitOfWork)
    {
        _logger = logger;
        _unitOfWork = unitOfWork;
    }

    public IActionResult Index()
    {
        IEnumerable<BookProduct> O_BookList = _unitOfWork.BookProduct.GetAll(includeProperties: "Category,ProductImages").ToList();

        return View(O_BookList.OrderBy(b => b.Id));
    }

    public IActionResult Details(int cartItemId)
    {
        ShoppingCart O_CartItem = new() {
            BookProduct = _unitOfWork.BookProduct.Get(c => c.Id == cartItemId, includeProperties: "Category,ProductImages"),
            CartItemId = cartItemId,
            Quantity = 1
        };        
        return View(O_CartItem);
    }

    [HttpPost]
    [Authorize]
    public IActionResult Details(ShoppingCart shoppingCart)
    {
        var claimsIdentity = (ClaimsIdentity)User.Identity;
        var userId = claimsIdentity.FindFirst(ClaimTypes.NameIdentifier).Value;

        shoppingCart.ApplicationUserId = userId;

        ShoppingCart cartFromDb = _unitOfWork.Cart.Get(c => c.ApplicationUserId == userId &&
                                                          c.CartItemId == shoppingCart.CartItemId, tracked: false);

        if ( cartFromDb is not null )
        {
            //ShoppingCart already exists
            cartFromDb.Quantity += shoppingCart.Quantity;
            _unitOfWork.Cart.Update(cartFromDb);
            _unitOfWork.Save();
        }
        else
        {
            // Add a Cart
            _unitOfWork.Cart.Add(shoppingCart);
            _unitOfWork.Save();
            HttpContext.Session.SetInt32(SD.SessionCart,
                            _unitOfWork.Cart.GetAll(u => u.ApplicationUserId == userId).Count());
        }

        TempData["success"] = "Cart updated successfully";


        return RedirectToAction(nameof(Index));
    }

    public IActionResult Privacy()
    {
        return View();
    }

    [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
    public IActionResult Error()
    {
        return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
    }
}
