using System.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using Bulky.Models;
using Bulky.Models.ViewModels;
using Bulky.DataAccess.Repository.IRepository;
using System.Security.Claims;
using Microsoft.AspNetCore.Authorization;
using Stripe.Checkout;
using Bulky.Utils;

namespace BulkyWeb.Areas.Customer.Controllers;

[Area("Customer")]
[Authorize]
public class CartController : Controller
{
    private readonly IUnitOfWork _unitOfWork;

    [BindProperty]
    public ShoppingCartVM shoppingCartVM { get; set; }

    public CartController(IUnitOfWork unitOfWork)
    {
            _unitOfWork = unitOfWork;
    }

    public IActionResult Index()
    {
        var claimsIdentity = (ClaimsIdentity)User.Identity;
        var userId = claimsIdentity.FindFirst(ClaimTypes.NameIdentifier);

        shoppingCartVM = new () {
            ShoppingCartList = _unitOfWork.Cart.GetAll(u => u.ApplicationUserId == userId.Value, includeProperties:"BookProduct"),
            OrderHeader = new()
        };


        IEnumerable<ProductImage> productImages = _unitOfWork.ProductImage.GetAll();
        //
        foreach(var cart in shoppingCartVM.ShoppingCartList)
        {
            cart.BookProduct.ProductImages = productImages.Where(u => u.ProductId == cart.BookProduct.Id).ToList();
            cart.Price = GetPriceBasedOnQuantity(cart);

            shoppingCartVM.OrderHeader.OrderTotal += cart.Price * cart.Quantity;
        }


        return View(shoppingCartVM);
    }

    public IActionResult Plus(int cartId)
    {
        var cartFromDb = _unitOfWork.Cart.Get(c => c.Id == cartId);
        cartFromDb.Quantity += 1;
        _unitOfWork.Cart.Update(cartFromDb);
        _unitOfWork.Save();

        return RedirectToAction(nameof(Index));
    }

    public IActionResult Minus(int cartId)
    {
        var cartFromDb = _unitOfWork.Cart.Get(c => c.Id == cartId);

        if (cartFromDb.Quantity <= 1)
        {
            _unitOfWork.Cart.Remove(cartFromDb);
            HttpContext.Session.SetInt32(SD.SessionCart,
                _unitOfWork.Cart.GetAll(u => u.ApplicationUserId == cartFromDb.ApplicationUserId).Count() - 1);
        }

        cartFromDb.Quantity -= 1;
        _unitOfWork.Cart.Update(cartFromDb);
        _unitOfWork.Save();

        return RedirectToAction(nameof(Index));
    }

    public IActionResult Remove(int cartId)
    {
        var cartFromDb = _unitOfWork.Cart.Get(c => c.Id == cartId);

        _unitOfWork.Cart.Remove(cartFromDb);
        HttpContext.Session.SetInt32(SD.SessionCart,
                _unitOfWork.Cart.GetAll(u => u.ApplicationUserId == cartFromDb.ApplicationUserId).Count() - 1);
        _unitOfWork.Save();

        return RedirectToAction(nameof(Index));
    }

    public IActionResult Summary()
    {
        var claimsIdentity = (ClaimsIdentity)User.Identity;
        var userId = claimsIdentity.FindFirst(ClaimTypes.NameIdentifier).Value;
        shoppingCartVM = new() {
            ShoppingCartList = _unitOfWork.Cart.GetAll(u => u.ApplicationUserId == userId, includeProperties:"BookProduct"),
            OrderHeader = new()
        };

        shoppingCartVM.OrderHeader.ApplicationUser = _unitOfWork.ApplicationUser.Get(u => u.Id == userId);


        shoppingCartVM.OrderHeader.Name = shoppingCartVM.OrderHeader.ApplicationUser.Name;
        shoppingCartVM.OrderHeader.PhoneNumber = shoppingCartVM.OrderHeader.ApplicationUser.PhoneNumber;
        shoppingCartVM.OrderHeader.StreetAddress = shoppingCartVM.OrderHeader.ApplicationUser.StreetAddress;
        shoppingCartVM.OrderHeader.City = shoppingCartVM.OrderHeader.ApplicationUser.City;
        shoppingCartVM.OrderHeader.State = shoppingCartVM.OrderHeader.ApplicationUser.State;
        shoppingCartVM.OrderHeader.PostalCode = shoppingCartVM.OrderHeader.ApplicationUser.PostalCode;

        foreach(var cart in shoppingCartVM.ShoppingCartList)
        {
            cart.Price = GetPriceBasedOnQuantity(cart);

            shoppingCartVM.OrderHeader.OrderTotal += cart.Price * cart.Quantity;
        }

        return View(shoppingCartVM);
    }

    [HttpPost]
    [ActionName("Summary")]
    public IActionResult SummaryPOST()
    {
        var claimsIdentity = (ClaimsIdentity)User.Identity;
        var userId = claimsIdentity.FindFirst(ClaimTypes.NameIdentifier).Value;

        shoppingCartVM.ShoppingCartList = _unitOfWork.Cart.GetAll(u => u.ApplicationUserId == userId, includeProperties:"BookProduct");

        shoppingCartVM.OrderHeader.OrderDate = System.DateTime.UtcNow;
        shoppingCartVM.OrderHeader.ApplicationUserId = userId;

        ApplicationUser applicationUser = _unitOfWork.ApplicationUser.Get(u => u.Id == userId);


        foreach(var cart in shoppingCartVM.ShoppingCartList)
        {
            cart.Price = GetPriceBasedOnQuantity(cart);

            shoppingCartVM.OrderHeader.OrderTotal += cart.Price * cart.Quantity;
        }

        if (applicationUser.CompanyId.GetValueOrDefault() == 0)
        {
            // It is a regular customer
            shoppingCartVM.OrderHeader.PaymentStatus = SD.PaymentStatusPending;
            shoppingCartVM.OrderHeader.OrderStatus = SD.StatusPending;

        }
        else
        {
            // it is a Company User
            shoppingCartVM.OrderHeader.PaymentStatus = SD.PaymentStatusDelayedPayment;
            shoppingCartVM.OrderHeader.OrderStatus = SD.StatusApproved;
        }

        _unitOfWork.OrderHeader.Add(shoppingCartVM.OrderHeader);
        _unitOfWork.Save();

        foreach(var cart in shoppingCartVM.ShoppingCartList)
        {
            OrderDetail orderDetail = new() {
                BookProductId = cart.CartItemId,
                OrderHeaderId = shoppingCartVM.OrderHeader.Id,
                Price = cart.Price,
                Quantity = cart.Quantity
            };
            _unitOfWork.OrderDetail.Add(orderDetail);
            _unitOfWork.Save();
        }

        if (applicationUser.CompanyId.GetValueOrDefault() == 0)
        {
            // It is a regular customer account and we need to capture payment
            // Stripe Logic
            var domain = Request.Scheme + "://" + Request.Host.Value;
            var options = new SessionCreateOptions
            {
                SuccessUrl = domain + $"/customer/cart/OrderConfirmation?id={shoppingCartVM.OrderHeader.Id}",
                CancelUrl = domain + $"/customer/cart/index",
                LineItems = new List<SessionLineItemOptions>(),
                Mode = "payment",
            };

            foreach(var item in shoppingCartVM.ShoppingCartList)
            {
               var sessionLineItem = new SessionLineItemOptions
               {
                   PriceData = new SessionLineItemPriceDataOptions
                   {
                       UnitAmount = (long)(item.Price*100),
                       Currency = "usd",
                       ProductData = new SessionLineItemPriceDataProductDataOptions
                       {
                           Name = item.BookProduct.Title
                       }
                   },
                   Quantity = item.Quantity
               };
               options.LineItems.Add(sessionLineItem);
            };

            var service = new SessionService();
            Session session = service.Create(options);

            _unitOfWork.OrderHeader.UpdateStripePaymentId(shoppingCartVM.OrderHeader.Id, session.Id, session.PaymentIntentId);
            _unitOfWork.Save();

            Response.Headers.Add("Location", session.Url);

            return new StatusCodeResult(303);

        }

        return RedirectToAction(nameof(OrderConfirmation), new { Id=shoppingCartVM.OrderHeader.Id });
    }


    public IActionResult OrderConfirmation(int id)
    {

        OrderHeader orderHeader = _unitOfWork.OrderHeader.Get(o => o.Id == id, includeProperties: "ApplicationUser");

        if(orderHeader.PaymentStatus != SD.PaymentStatusDelayedPayment)
        {
            // This is an order by a customer

            var service = new SessionService();

            Session session = service.Get(orderHeader.SessionId);

            if(session.PaymentStatus.ToLower() == "paid" )
            {

                _unitOfWork.OrderHeader.UpdateStripePaymentId(id, session.Id, session.PaymentIntentId);
                _unitOfWork.OrderHeader.UpdateStatus(id, SD.StatusApproved, SD.PaymentStatusApproved);
                _unitOfWork.Save();

                HttpContext.Session.Clear();

            }

            List<ShoppingCart> shoppingCarts = _unitOfWork.Cart.GetAll(u => u.ApplicationUserId == orderHeader.ApplicationUserId).ToList();
            _unitOfWork.Cart.RemoveRange(shoppingCarts);
            _unitOfWork.Save();

        }

        return View(id);
    }

    private double GetPriceBasedOnQuantity(ShoppingCart shoppingCart)
    {
        if (shoppingCart.Quantity <= 50)
        {
            return shoppingCart.BookProduct.Price;
        }
        else
        {
            if (shoppingCart.Quantity <= 100)
            {
                return shoppingCart.BookProduct.Price50;
            }
            else
            {
                return shoppingCart.BookProduct.Price100;
            }
        }
    }
}
