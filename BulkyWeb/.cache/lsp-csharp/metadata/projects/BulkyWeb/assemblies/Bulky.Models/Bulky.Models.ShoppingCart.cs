using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.CompilerServices;
using Microsoft.AspNetCore.Mvc.ModelBinding.Validation;

namespace Bulky.Models;

public class ShoppingCart
{
	[Key]
	public int Id
	{
		[CompilerGenerated]
		get
		{
			throw null;
		}
		[CompilerGenerated]
		set
		{
			throw null;
		}
	}

	[Range(1, 1000, ErrorMessage = "Please enter a value betwen 1 an 1000")]
	public int Quantity
	{
		[CompilerGenerated]
		get
		{
			throw null;
		}
		[CompilerGenerated]
		set
		{
			throw null;
		}
	}

	public int CartItemId
	{
		[CompilerGenerated]
		get
		{
			throw null;
		}
		[CompilerGenerated]
		set
		{
			throw null;
		}
	}

	[ForeignKey("CartItemId")]
	[ValidateNever]
	public BookProduct? BookProduct
	{
		[CompilerGenerated]
		get
		{
			throw null;
		}
		[CompilerGenerated]
		set
		{
			throw null;
		}
	}

	public string? ApplicationUserId
	{
		[CompilerGenerated]
		get
		{
			throw null;
		}
		[CompilerGenerated]
		set
		{
			throw null;
		}
	}

	[ForeignKey("ApplicationUserId")]
	[ValidateNever]
	public ApplicationUser? ApplicationUser
	{
		[CompilerGenerated]
		get
		{
			throw null;
		}
		[CompilerGenerated]
		set
		{
			throw null;
		}
	}

	[NotMapped]
	public double Price
	{
		[CompilerGenerated]
		get
		{
			throw null;
		}
		[CompilerGenerated]
		set
		{
			throw null;
		}
	}

	public ShoppingCart()
	{
		throw null;
	}
}
