using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Mvc.ViewFeatures;

namespace Microsoft.AspNetCore.Mvc;

/// <summary>
/// A base class for an MVC controller with view support.
/// </summary>
public abstract class Controller : ControllerBase, IActionFilter, IFilterMetadata, IAsyncActionFilter, IDisposable
{
	/// <summary>
	/// Gets or sets <see cref="T:Microsoft.AspNetCore.Mvc.ViewFeatures.ViewDataDictionary" /> used by <see cref="T:Microsoft.AspNetCore.Mvc.ViewResult" /> and <see cref="P:Microsoft.AspNetCore.Mvc.Controller.ViewBag" />.
	/// </summary>
	/// <remarks>
	/// By default, this property is initialized when <see cref="T:Microsoft.AspNetCore.Mvc.Controllers.IControllerActivator" /> activates
	/// controllers.
	/// <para>
	/// This property can be accessed after the controller has been activated, for example, in a controller action
	/// or by overriding <see cref="M:Microsoft.AspNetCore.Mvc.Controller.OnActionExecuting(Microsoft.AspNetCore.Mvc.Filters.ActionExecutingContext)" />.
	/// </para>
	/// <para>
	/// This property can be also accessed from within a unit test where it is initialized with
	/// <see cref="T:Microsoft.AspNetCore.Mvc.ModelBinding.EmptyModelMetadataProvider" />.
	/// </para>
	/// </remarks>
	[ViewDataDictionary]
	public ViewDataDictionary ViewData
	{
		get
		{
			throw null;
		}
		set
		{
			throw null;
		}
	}

	/// <summary>
	/// Gets or sets <see cref="T:Microsoft.AspNetCore.Mvc.ViewFeatures.ITempDataDictionary" /> used by <see cref="T:Microsoft.AspNetCore.Mvc.ViewResult" />.
	/// </summary>
	public ITempDataDictionary TempData
	{
		get
		{
			throw null;
		}
		set
		{
			throw null;
		}
	}

	/// <summary>
	/// Gets the dynamic view bag.
	/// </summary>
	public dynamic ViewBag
	{
		get
		{
			throw null;
		}
	}

	/// <summary>
	/// Creates a <see cref="T:Microsoft.AspNetCore.Mvc.ViewResult" /> object that renders a view to the response.
	/// </summary>
	/// <returns>The created <see cref="T:Microsoft.AspNetCore.Mvc.ViewResult" /> object for the response.</returns>
	[NonAction]
	public virtual ViewResult View()
	{
		throw null;
	}

	/// <summary>
	/// Creates a <see cref="T:Microsoft.AspNetCore.Mvc.ViewResult" /> object by specifying a <paramref name="viewName" />.
	/// </summary>
	/// <param name="viewName">The name or path of the view that is rendered to the response.</param>
	/// <returns>The created <see cref="T:Microsoft.AspNetCore.Mvc.ViewResult" /> object for the response.</returns>
	[NonAction]
	public virtual ViewResult View(string? viewName)
	{
		throw null;
	}

	/// <summary>
	/// Creates a <see cref="T:Microsoft.AspNetCore.Mvc.ViewResult" /> object by specifying a <paramref name="model" />
	/// to be rendered by the view.
	/// </summary>
	/// <param name="model">The model that is rendered by the view.</param>
	/// <returns>The created <see cref="T:Microsoft.AspNetCore.Mvc.ViewResult" /> object for the response.</returns>
	[NonAction]
	public virtual ViewResult View(object? model)
	{
		throw null;
	}

	/// <summary>
	/// Creates a <see cref="T:Microsoft.AspNetCore.Mvc.ViewResult" /> object by specifying a <paramref name="viewName" />
	/// and the <paramref name="model" /> to be rendered by the view.
	/// </summary>
	/// <param name="viewName">The name or path of the view that is rendered to the response.</param>
	/// <param name="model">The model that is rendered by the view.</param>
	/// <returns>The created <see cref="T:Microsoft.AspNetCore.Mvc.ViewResult" /> object for the response.</returns>
	[NonAction]
	public virtual ViewResult View(string? viewName, object? model)
	{
		throw null;
	}

	/// <summary>
	/// Creates a <see cref="T:Microsoft.AspNetCore.Mvc.PartialViewResult" /> object that renders a partial view to the response.
	/// </summary>
	/// <returns>The created <see cref="T:Microsoft.AspNetCore.Mvc.PartialViewResult" /> object for the response.</returns>
	[NonAction]
	public virtual PartialViewResult PartialView()
	{
		throw null;
	}

	/// <summary>
	/// Creates a <see cref="T:Microsoft.AspNetCore.Mvc.PartialViewResult" /> object by specifying a <paramref name="viewName" />.
	/// </summary>
	/// <param name="viewName">The name or path of the partial view that is rendered to the response.</param>
	/// <returns>The created <see cref="T:Microsoft.AspNetCore.Mvc.PartialViewResult" /> object for the response.</returns>
	[NonAction]
	public virtual PartialViewResult PartialView(string? viewName)
	{
		throw null;
	}

	/// <summary>
	/// Creates a <see cref="T:Microsoft.AspNetCore.Mvc.PartialViewResult" /> object by specifying a <paramref name="model" />
	/// to be rendered by the partial view.
	/// </summary>
	/// <param name="model">The model that is rendered by the partial view.</param>
	/// <returns>The created <see cref="T:Microsoft.AspNetCore.Mvc.PartialViewResult" /> object for the response.</returns>
	[NonAction]
	public virtual PartialViewResult PartialView(object? model)
	{
		throw null;
	}

	/// <summary>
	/// Creates a <see cref="T:Microsoft.AspNetCore.Mvc.PartialViewResult" /> object by specifying a <paramref name="viewName" />
	/// and the <paramref name="model" /> to be rendered by the partial view.
	/// </summary>
	/// <param name="viewName">The name or path of the partial view that is rendered to the response.</param>
	/// <param name="model">The model that is rendered by the partial view.</param>
	/// <returns>The created <see cref="T:Microsoft.AspNetCore.Mvc.PartialViewResult" /> object for the response.</returns>
	[NonAction]
	public virtual PartialViewResult PartialView(string? viewName, object? model)
	{
		throw null;
	}

	/// <summary>
	/// Creates a <see cref="T:Microsoft.AspNetCore.Mvc.ViewComponentResult" /> by specifying the name of a view component to render.
	/// </summary>
	/// <param name="componentName">
	/// The view component name. Can be a view component
	/// <see cref="P:Microsoft.AspNetCore.Mvc.ViewComponents.ViewComponentDescriptor.ShortName" /> or
	/// <see cref="P:Microsoft.AspNetCore.Mvc.ViewComponents.ViewComponentDescriptor.FullName" />.</param>
	/// <returns>The created <see cref="T:Microsoft.AspNetCore.Mvc.ViewComponentResult" /> object for the response.</returns>
	[NonAction]
	public virtual ViewComponentResult ViewComponent(string componentName)
	{
		throw null;
	}

	/// <summary>
	/// Creates a <see cref="T:Microsoft.AspNetCore.Mvc.ViewComponentResult" /> by specifying the <see cref="T:System.Type" /> of a view component to
	/// render.
	/// </summary>
	/// <param name="componentType">The view component <see cref="T:System.Type" />.</param>
	/// <returns>The created <see cref="T:Microsoft.AspNetCore.Mvc.ViewComponentResult" /> object for the response.</returns>
	[NonAction]
	public virtual ViewComponentResult ViewComponent(Type componentType)
	{
		throw null;
	}

	/// <summary>
	/// Creates a <see cref="T:Microsoft.AspNetCore.Mvc.ViewComponentResult" /> by specifying the name of a view component to render.
	/// </summary>
	/// <param name="componentName">
	/// The view component name. Can be a view component
	/// <see cref="P:Microsoft.AspNetCore.Mvc.ViewComponents.ViewComponentDescriptor.ShortName" /> or
	/// <see cref="P:Microsoft.AspNetCore.Mvc.ViewComponents.ViewComponentDescriptor.FullName" />.</param>
	/// <param name="arguments">
	/// An <see cref="T:System.Object" /> with properties representing arguments to be passed to the invoked view component
	/// method. Alternatively, an <see cref="T:System.Collections.Generic.IDictionary`2" /> instance
	/// containing the invocation arguments.
	/// </param>
	/// <returns>The created <see cref="T:Microsoft.AspNetCore.Mvc.ViewComponentResult" /> object for the response.</returns>
	[NonAction]
	public virtual ViewComponentResult ViewComponent(string componentName, object? arguments)
	{
		throw null;
	}

	/// <summary>
	/// Creates a <see cref="T:Microsoft.AspNetCore.Mvc.ViewComponentResult" /> by specifying the <see cref="T:System.Type" /> of a view component to
	/// render.
	/// </summary>
	/// <param name="componentType">The view component <see cref="T:System.Type" />.</param>
	/// <param name="arguments">
	/// An <see cref="T:System.Object" /> with properties representing arguments to be passed to the invoked view component
	/// method. Alternatively, an <see cref="T:System.Collections.Generic.IDictionary`2" /> instance
	/// containing the invocation arguments.
	/// </param>
	/// <returns>The created <see cref="T:Microsoft.AspNetCore.Mvc.ViewComponentResult" /> object for the response.</returns>
	[NonAction]
	public virtual ViewComponentResult ViewComponent(Type componentType, object? arguments)
	{
		throw null;
	}

	/// <summary>
	/// Creates a <see cref="T:Microsoft.AspNetCore.Mvc.JsonResult" /> object that serializes the specified <paramref name="data" /> object
	/// to JSON.
	/// </summary>
	/// <param name="data">The object to serialize.</param>
	/// <returns>The created <see cref="T:Microsoft.AspNetCore.Mvc.JsonResult" /> that serializes the specified <paramref name="data" />
	/// to JSON format for the response.</returns>
	[NonAction]
	public virtual JsonResult Json(object? data)
	{
		throw null;
	}

	/// <summary>
	/// Creates a <see cref="T:Microsoft.AspNetCore.Mvc.JsonResult" /> object that serializes the specified <paramref name="data" /> object
	/// to JSON.
	/// </summary>
	/// <param name="data">The object to serialize.</param>
	/// <param name="serializerSettings">The serializer settings to be used by the formatter.
	/// <para>
	/// When using <c>System.Text.Json</c>, this should be an instance of <see cref="T:System.Text.Json.JsonSerializerOptions" />.
	/// </para>
	/// <para>
	/// When using <c>Newtonsoft.Json</c>, this should be an instance of <c>JsonSerializerSettings</c>.
	/// </para>
	/// </param>
	/// <returns>The created <see cref="T:Microsoft.AspNetCore.Mvc.JsonResult" /> that serializes the specified <paramref name="data" />
	/// as JSON format for the response.</returns>
	/// <remarks>Callers should cache an instance of serializer settings to avoid
	/// recreating cached data with each call.</remarks>
	[NonAction]
	public virtual JsonResult Json(object? data, object? serializerSettings)
	{
		throw null;
	}

	/// <summary>
	/// Called before the action method is invoked.
	/// </summary>
	/// <param name="context">The action executing context.</param>
	[NonAction]
	public virtual void OnActionExecuting(ActionExecutingContext context)
	{
		throw null;
	}

	/// <summary>
	/// Called after the action method is invoked.
	/// </summary>
	/// <param name="context">The action executed context.</param>
	[NonAction]
	public virtual void OnActionExecuted(ActionExecutedContext context)
	{
		throw null;
	}

	/// <summary>
	/// Called before the action method is invoked.
	/// </summary>
	/// <param name="context">The action executing context.</param>
	/// <param name="next">The <see cref="T:Microsoft.AspNetCore.Mvc.Filters.ActionExecutionDelegate" /> to execute. Invoke this delegate in the body
	/// of <see cref="M:Microsoft.AspNetCore.Mvc.Controller.OnActionExecutionAsync(Microsoft.AspNetCore.Mvc.Filters.ActionExecutingContext,Microsoft.AspNetCore.Mvc.Filters.ActionExecutionDelegate)" /> to continue execution of the action.</param>
	/// <returns>A <see cref="T:System.Threading.Tasks.Task" /> instance.</returns>
	[NonAction]
	public virtual Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
	{
		throw null;
	}

	/// <inheritdoc />
	public void Dispose()
	{
		throw null;
	}

	/// <summary>
	/// Releases all resources currently used by this <see cref="T:Microsoft.AspNetCore.Mvc.Controller" /> instance.
	/// </summary>
	/// <param name="disposing"><c>true</c> if this method is being invoked by the <see cref="M:Microsoft.AspNetCore.Mvc.Controller.Dispose" /> method,
	/// otherwise <c>false</c>.</param>
	protected virtual void Dispose(bool disposing)
	{
		throw null;
	}

	protected Controller()
	{
		throw null;
	}
}
