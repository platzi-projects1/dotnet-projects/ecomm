using Microsoft.EntityFrameworkCore;

using Bulky.DataAccess.Data;
using Bulky.DataAccess.DbInitializer;
using Bulky.Utils;
using Bulky.DataAccess.Repository;
using Bulky.DataAccess.Repository.IRepository;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.UI.Services;

// using System.Configuration;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Services.AddControllersWithViews();

var PgConn = builder.Configuration["ConnectionString:EComm"];
var StripeConn = builder.Configuration["BulkyBook:StripeSK"];

IConfigurationRoot config = new ConfigurationBuilder()
    .AddJsonFile("appsettings.Production.json")
    .AddEnvironmentVariables()
    .Build();

AppSecrets appSecConf = new AppSecretConfiguration(config).AccessConfig();

AzureKeyVaultSettings azureKeyVaultSettings = new();

KeyVaultSecrets kvSecrets = new (appSecConf, azureKeyVaultSettings);

if (builder.Environment.IsProduction())
{
    var client = kvSecrets.GetClient();

    var getAllSecrets = kvSecrets.GetSecrets(client);

    builder.Services.AddDbContext<ApplicationDbContext>(options =>
    options.UseNpgsql(getAllSecrets.DbConn));

    builder.Services.AddAuthentication().AddFacebook(options => {
        options.AppId = getAllSecrets.FbAppID;
        options.AppSecret = getAllSecrets.FbAppSecret;
    });

    builder.Services.AddAuthentication().AddMicrosoftAccount(options => {
        options.ClientId = getAllSecrets.EntraClienID;
        options.ClientSecret = getAllSecrets.EntraClienSecret;
    });

    Stripe.StripeConfiguration.ApiKey = getAllSecrets.StripeSKey;
} 
else if (builder.Environment.IsDevelopment())
{
    builder.Services.AddDbContext<ApplicationDbContext>(options =>
        options.UseNpgsql(PgConn));

    Stripe.StripeConfiguration.ApiKey = StripeConn;
}


builder.Services.AddIdentity<IdentityUser, IdentityRole>()
    .AddEntityFrameworkStores<ApplicationDbContext>().AddDefaultTokenProviders();


builder.Services.ConfigureApplicationCookie(options => {
    options.LoginPath = $"/Identity/Account/Login";
    options.LogoutPath = $"/Identity/Account/Logout";
    options.AccessDeniedPath = $"/Identity/Account/AccessDenied";
});

builder.Services.AddDistributedMemoryCache();
builder.Services.AddSession(options => {
    options.IdleTimeout = TimeSpan.FromMinutes(100);
    options.Cookie.HttpOnly = true;
    options.Cookie.IsEssential = true;
});


builder.Services.AddScoped<IUnitOfWork, UnitOfWork>();

builder.Services.AddScoped<IEmailSender, EmailSender>();

builder.Services.AddControllers().AddJsonOptions(options => {
    options.JsonSerializerOptions.Converters.Add(new DateOnlyJsonConverter());
});

builder.Services.AddScoped<IDbInitializer, DbInitializer>();
builder.Services.AddRazorPages();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (!app.Environment.IsDevelopment())
{
    app.UseExceptionHandler("/Home/Error");
    // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
    app.UseHsts();
}

app.UseHttpsRedirection();
app.UseStaticFiles();



app.UseRouting();
app.UseAuthentication();

app.UseAuthorization();
app.UseSession();
SeedDatabase();
app.MapRazorPages();
app.MapControllerRoute(
    name: "default",
    pattern: "{area=Customer}/{controller=Home}/{action=Index}/{id?}");

app.Run();

void SeedDatabase() {
    using (var scope = app.Services.CreateScope()) {
        var dbInitializer = scope.ServiceProvider.GetRequiredService<IDbInitializer>();
        dbInitializer.Initialize();
    }
}
