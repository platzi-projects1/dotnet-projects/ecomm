namespace Bulky.Utils
{
    public class AppSecrets
    {
            public string DbConnection { get; set; } = string.Empty;
            public string AzureKeyVaultUrl { get; set; } = string.Empty;
            public string EntraClientID { get; set; } = string.Empty;
            public string EntraClientSecret { get; set; } = string.Empty;
            public string FacebookAppId { get; set; } = string.Empty;
            public string FacebookAppSecret { get; set; } = string.Empty;
            public string StripeSKey { get; set; } = string.Empty;
            public string StripePKey { get; set; } = string.Empty;

    }
}
