using Microsoft.Extensions.Configuration;

namespace Bulky.Utils
{
    public class AppSecretConfiguration
    {


        private readonly IConfiguration _config;

        public AppSecretConfiguration(IConfiguration config)
        {
            _config = config;
        }

        public AppSecrets AccessConfig()
        {
            IConfigurationSection appSecretsSection = _config.GetSection("AppServices");

            AppSecrets appSecrets = new() {
                DbConnection = appSecretsSection.GetValue<string>("DbConnection"),
                AzureKeyVaultUrl = appSecretsSection.GetValue<string>("KeyVault:KeyVaultUrl"),
                EntraClientID = appSecretsSection.GetValue<string>("Entra:EntraClientID"),
                EntraClientSecret = appSecretsSection.GetValue<string>("Entra:EntraClientSecret"),
                FacebookAppId = appSecretsSection.GetValue<string>("Facebook:AppId"),
                FacebookAppSecret = appSecretsSection.GetValue<string>("Facebook:AppSecret"),
                StripeSKey = appSecretsSection.GetValue<string>("Stripe:StripeSK"),
                StripePKey = appSecretsSection.GetValue<string>("Stripe:StripePK"),
            };
            return appSecrets;
        }

    }
}
