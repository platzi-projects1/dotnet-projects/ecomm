namespace Bulky.Utils
{
    public class AzureKeyVaultSettings
    {
        public string DbConn { get; set; } = string.Empty;
        public string FbAppID { get; set; } = string.Empty;
        public string FbAppSecret { get; set; } = string.Empty;
        public string EntraClienID { get; set; } = string.Empty;
        public string EntraClienSecret { get; set; } = string.Empty;
        public string StripeSKey { get; set; } = string.Empty;
        public string StripePKey { get; set; } = string.Empty;
    }
}
