using Microsoft.AspNetCore.Identity.UI.Services;

namespace Bulky.Utils;

public class EmailSender : IEmailSender
{
    public Task SendEmailAsync(string email, string subject, string htmlmessage)
    {
        // Logic to send an Email
        return Task.CompletedTask;
    }
}
