using Azure.Identity;
using Azure.Security.KeyVault.Secrets;
using Azure.Core;

namespace Bulky.Utils
{
    public class KeyVaultSecrets
    {

        private readonly AppSecrets _appSecrets;
        private readonly AzureKeyVaultSettings _kvSettings;


        public KeyVaultSecrets(AppSecrets appSecrets, AzureKeyVaultSettings kvSettings){
            _appSecrets = appSecrets;
            _kvSettings = kvSettings;
        }

        public SecretClient GetClient()
        {

            SecretClientOptions options = new SecretClientOptions()
            {
                Retry =
                {
                    Delay= TimeSpan.FromSeconds(2),
                    MaxDelay = TimeSpan.FromSeconds(16),
                    MaxRetries = 5,
                    Mode = RetryMode.Exponential
                }
            };

            var client = new SecretClient(new Uri(_appSecrets.AzureKeyVaultUrl), new DefaultAzureCredential(),options);

            return client;

        }

        public AzureKeyVaultSettings GetSecrets(SecretClient client)
        {
        //    var client = this.GetClient();

           _kvSettings.DbConn = client.GetSecret(_appSecrets.DbConnection).Value.Value.ToString();

           _kvSettings.FbAppID = client.GetSecret(_appSecrets.FacebookAppId).Value.Value.ToString();
           _kvSettings.FbAppSecret = client.GetSecret(_appSecrets.FacebookAppSecret).Value.Value.ToString();

           _kvSettings.EntraClienID = client.GetSecret(_appSecrets.EntraClientID).Value.Value.ToString();
           _kvSettings.EntraClienSecret = client.GetSecret(_appSecrets.EntraClientSecret).Value.Value.ToString();

           _kvSettings.StripeSKey = client.GetSecret(_appSecrets.StripeSKey).Value.Value.ToString();
           _kvSettings.StripePKey = client.GetSecret(_appSecrets.StripePKey).Value.Value.ToString();

           return _kvSettings;
        }

    }
}
