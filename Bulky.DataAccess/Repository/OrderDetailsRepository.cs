using Bulky.Models;
using Bulky.DataAccess.Data;
using Bulky.DataAccess.Repository.IRepository;

namespace Bulky.DataAccess.Repository;

public class OrderDetailsRepository : Repository<OrderDetail>, IOrderDetailsRepository
{
private readonly ApplicationDbContext _db;

    public OrderDetailsRepository(ApplicationDbContext db) : base(db)
    {
        _db = db;
    }

    public void Update(OrderDetail O_OrderDetail)
    {
        var objFromDb = _db.OrderDetails.FirstOrDefault(o => o.Id == O_OrderDetail.Id);

        if (objFromDb is not null)
        {
           _db.Update(objFromDb);
        }
    }
}
