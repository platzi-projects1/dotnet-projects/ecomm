using Bulky.DataAccess.Data;
using Bulky.Models;

using Bulky.DataAccess.Repository.IRepository;

namespace Bulky.DataAccess.Repository
{

    public class ProductImageRepository : Repository<ProductImage>, IProductImageRepository
    {

        private readonly ApplicationDbContext _db;

        public ProductImageRepository(ApplicationDbContext db) : base(db)
        {
            _db = db;
        }

        public void Update(ProductImage O_ProdImg)
        {
            _db.Update(O_ProdImg);
        }
    }


}
