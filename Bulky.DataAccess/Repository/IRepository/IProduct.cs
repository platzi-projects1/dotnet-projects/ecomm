using Bulky.Models;

namespace Bulky.DataAccess.Repository.IRepository
{
    public interface IBookProductRepository : IRepository<BookProduct>
    {
        public void Update(BookProduct O_Book);
    }
}
