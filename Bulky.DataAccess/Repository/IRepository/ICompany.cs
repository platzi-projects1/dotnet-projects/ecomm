using Bulky.Models;

namespace Bulky.DataAccess.Repository.IRepository;

public interface ICompanyRepository : IRepository<Company>
{
   public void Update(Company O_Company);
}
