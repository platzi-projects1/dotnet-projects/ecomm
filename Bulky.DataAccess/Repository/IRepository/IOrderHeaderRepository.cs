using Bulky.Models;

namespace Bulky.DataAccess.Repository.IRepository;

public interface IOrderHeaderRepository: IRepository<OrderHeader>
{
    public void Update(OrderHeader O_OrderHeader);
    public void UpdateStatus(int id, string orderStatus, string? paymentStatus = null);
    public void UpdateStripePaymentId(int id, string sessionId, string paymentIntentId);
}
