using Bulky.Models;

namespace Bulky.DataAccess.Repository.IRepository;

public interface IOrderDetailsRepository: IRepository<OrderDetail>
{
    public void Update(OrderDetail O_OrderDetail);
}
