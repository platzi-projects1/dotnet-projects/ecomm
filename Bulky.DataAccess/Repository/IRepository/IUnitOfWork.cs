namespace Bulky.DataAccess.Repository.IRepository
{
    public interface IUnitOfWork
    {
        public IApplicationUserRepository ApplicationUser { get; }
        public ICategoryRepository Category { get; }
        public IBookProductRepository BookProduct { get; }
        public IProductImageRepository ProductImage { get; }
        public ICompanyRepository Company { get; }
        public IShoppingCartRepository Cart { get; }
        public IOrderHeaderRepository OrderHeader { get; }
        public IOrderDetailsRepository OrderDetail { get; }
        public void Save();
    }
}
