using Bulky.Models;

namespace Bulky.DataAccess.Repository.IRepository
{
    public interface IProductImageRepository : IRepository<ProductImage>
    {
        public void Update(ProductImage O_ProductImage);
    }
}
