using Bulky.DataAccess.Data;
using Bulky.Models;

using Bulky.DataAccess.Repository.IRepository;

namespace Bulky.DataAccess.Repository
{

    public class CategoryRepository : Repository<Category>, ICategoryRepository
    {

        private readonly ApplicationDbContext _db;

        public CategoryRepository(ApplicationDbContext db) : base(db)
        {
            _db = db;
        }

        public void Update(Category O_Cat)
        {
            _db.Update(O_Cat);
        }
    }


}
