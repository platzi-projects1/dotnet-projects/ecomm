using Bulky.DataAccess.Data;
using Bulky.Models;
using Bulky.DataAccess.Repository.IRepository;

namespace Bulky.DataAccess.Repository
{
    public class BookProductRepository : Repository<BookProduct>, IBookProductRepository
    {

        private readonly ApplicationDbContext _db;

        public BookProductRepository(ApplicationDbContext db) : base(db)
        {
            _db = db;
        }

        public void Update(BookProduct O_Book)
        {
            var objFromDb = _db.BookProducts.FirstOrDefault(o => o.Id == O_Book.Id);

            if (objFromDb is not null)
            {
                objFromDb.Title = O_Book.Title;
                objFromDb.Author = O_Book.Author;
                objFromDb.Description = O_Book.Description;
                objFromDb.ISBN = O_Book.ISBN;
                objFromDb.ListPrice = O_Book.ListPrice;
                objFromDb.Price = O_Book.Price;
                objFromDb.Price50 = O_Book.Price50;
                objFromDb.Price100 = O_Book.Price100;
                objFromDb.CategoryId = O_Book.CategoryId;
                objFromDb.ProductImages = O_Book.ProductImages;

            }
        }
    }
}
