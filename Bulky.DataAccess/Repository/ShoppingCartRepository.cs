using Bulky.Models;
using Bulky.DataAccess.Data;
using Bulky.DataAccess.Repository.IRepository;

namespace Bulky.DataAccess.Repository;

public class ShoppingCartRepository : Repository<ShoppingCart>, IShoppingCartRepository
{
private readonly ApplicationDbContext _db;

    public ShoppingCartRepository(ApplicationDbContext db) : base(db)
    {
        _db = db;
    }

    public void Update(ShoppingCart O_Cart)
    {
        var objFromDb = _db.ShoppingCarts.FirstOrDefault(o => o.Id == O_Cart.Id);

        if (objFromDb is not null)
        {
            if ( objFromDb.Quantity >= 0 )
            {
                objFromDb.Quantity = O_Cart.Quantity;
            }
        }
    }
}
