using Bulky.DataAccess.Data;
using Bulky.Models;
using Bulky.DataAccess.Repository.IRepository;

namespace Bulky.DataAccess.Repository;

public class CompanyRepository : Repository<Company>, ICompanyRepository
{
    private readonly ApplicationDbContext _db;

    public CompanyRepository(ApplicationDbContext db) : base(db)
    {
        _db = db;
    }

    public void Update(Company O_Company)
    {
        var objFromDb = _db.Companies.FirstOrDefault(o => o.Id == O_Company.Id);

        if (objFromDb is not null)
        {
            objFromDb.Name = O_Company.Name;
            objFromDb.StreetAddress = O_Company.StreetAddress;
            objFromDb.PhoneNumber = O_Company.PhoneNumber;
            objFromDb.City = O_Company.City;
            objFromDb.State = O_Company.State;
            objFromDb.PostalCode = O_Company.PostalCode;
        }
    }

}
