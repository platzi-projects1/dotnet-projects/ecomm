using Microsoft.AspNetCore.Mvc.ModelBinding.Validation;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Bulky.Models;

public class ShoppingCart
{
    [Key]
    public int Id { get; set; }

    [Range(1, 1000, ErrorMessage = "Please enter a value betwen 1 an 1000")]
    public int Quantity { get; set; }

    public int CartItemId { get; set; }
    [ForeignKey("CartItemId")]
    [ValidateNever]
    public BookProduct? BookProduct { get; set; }

    public string? ApplicationUserId { get; set; }
    [ForeignKey("ApplicationUserId")]
    [ValidateNever]
    public ApplicationUser? ApplicationUser { get; set; }

    [NotMapped]
    public double Price { get; set; }
}
