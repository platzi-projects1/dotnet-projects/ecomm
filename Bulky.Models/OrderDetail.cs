using Microsoft.AspNetCore.Mvc.ModelBinding.Validation;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Bulky.Models;

public class OrderDetail
{
    public int Id { get; set; }

    [Required]
    public int OrderHeaderId { get; set; }

    [ForeignKey("OrderHeaderId")]
    [ValidateNever]
    public OrderHeader OrderHeader { get; set; }

    [Required]
    public int BookProductId { get; set; }

    [ForeignKey("BookProductId")]
    [ValidateNever]
    public BookProduct BookProduct { get; set; }


    public int Quantity { get; set; }
    public double Price { get; set; }

}
