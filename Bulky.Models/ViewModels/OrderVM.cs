namespace Bulky.Models.ViewModels
{
    public class OrderVM
    {
        public IEnumerable<OrderDetail> OOrderDetailList { get; set; }
        public OrderHeader? OrderHeader { get; set; }
    }
}
